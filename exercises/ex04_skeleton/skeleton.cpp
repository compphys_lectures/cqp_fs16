#include <vector>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cassert>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/random.hpp>
#include <boost/tuple/tuple.hpp>

class Hamiltonian
{
public:
  typedef double Scalar;
  typedef boost::numeric::ublas::vector<Scalar> Vector;

  // Dimension of Vector Space
  unsigned dimension() const;

  // Calculate y = H x
  void multiply(const Vector& x, Vector& y) const;

  // ...
};

namespace ietl
{
	/* here we include a "mult" function in the ietl name space specifically designed for use with our Hamiltonian class objects.  */
  inline void mult( const Hamiltonian& h, const Hamiltonian::Vector& x, Hamiltonian::Vector& y )
  {
    h.multiply( x, y );
	/*To minimize your memory requirements you could implement this method "on the fly", i.e. in a way that never constructs the hamitonian matrix explicitly. */
  }
}

// ietl::mult() needs to be declared before including these
#include <ietl/interface/ublas.h>
#include <ietl/lanczos.h>

std::pair< std::vector<double>, std::vector<int> >
diagonalize( const Hamiltonian& h, unsigned nvals=1, unsigned maxiter=1000)
{
  typedef ietl::vectorspace<Hamiltonian::Vector> Vectorspace;
  typedef ietl::lanczos<Hamiltonian,Vectorspace> Lanczos;
  typedef ietl::lanczos_iteration_nlowest<double> Iteration;
  typedef boost::mt19937 Generator; 

  Vectorspace vspace(h.dimension());
  Lanczos solver(h,vspace);
  Iteration iter(maxiter,nvals); /* maxiter limits the number of Lanczos iterations and nvals tells the solver the number of eigenvalues to calculate.*/
  solver.calculate_eigenvalues(iter,Generator()); /* Generator() creates a pseudo-random number generator (Mersenne-Twister) used for starting the Lanczos algorithm. */

  if( iter.iterations() == maxiter )
    std::cerr << "Lanczos did not converge in " << iter.iterations() << " iterations." << std::endl;
  else
    std::cout << "  Lanczos converged after " << iter.iterations() << " iterations." << std::endl;

  return std::make_pair(solver.eigenvalues(),solver.multiplicities());
}

