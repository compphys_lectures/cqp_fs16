#ifndef SIM_QUANTUM_ANNEALER_HPP
#define SIM_QUANTUM_ANNEALER_HPP
#include <vector>
#include <limits>
#include <random>
#include <assert.h>

namespace cqp {

class quantum_annealer {
  public:
    typedef int energy_t;

    quantum_annealer(std::size_t N, std::vector<short> const& row_c, std::vector<short> const& col_c, std::size_t M, unsigned int seed = 3000)
    : rng(seed), random_site(0,N-1), random_time_slice(0,M-1), row_c(row_c), col_c(col_c), spin_rows(N*M), N(N), M(M) {
        // We also assume row_c and col_c have only +1,-1 elements
        assert(N < 64);
        init();
    }

    void init() {
        // Generate random spin config
        for(std::size_t i = 0; i < spin_rows.size(); ++i) {
            spin_rows[i] = rng();
            spin_rows[i] <<= 32;
            spin_rows[i] |= rng();
        }
    }

    // +1 if spins are antialigned, -1 if spins are aligned
    inline int64_t get_sign(std::size_t i1, std::size_t j1, std::size_t i2, std::size_t j2) const {
        assert(i1 < spin_rows.size());
        assert(i2 < spin_rows.size());
        assert(j1 < 64);
        assert(j2 < 64);
        return 2*(((spin_rows[i1] >> j1) ^ (spin_rows[i2] >> (j2))) & int64_t(1))-1;
    }

    void sweep(double beta, double alpha, double field) {

        double delta = beta / M;
        double j_t = std::numeric_limits<double>::max(); // limit of the coupling strength as gamma ->0 (all the replicas are perfectly coupled)
        if(field > 0.0)
            j_t = -0.5*std::log(0.5*delta*field);  // the inner factor 0.5 might be wrong (in a harmless way)
        // 9 possible energies real space
        // * 3 possible settings of the neighboring
        // spins in imaginary time
        //  real space acceptance probabilities.
        double rs_acc_prop[9];
        for(int i=0; i < 9; ++i)
            rs_acc_prop[i] = std::exp( 2*alpha*delta*(4-i));
        // imaginary time acceptance probabilities.
        double it_acc_prop[3];
        for(int i=0; i < 3; ++i)
            it_acc_prop[i] = std::exp( -(1-i)*2*2 * j_t);

        for(std::size_t n = 0; n < N*N*M; ++n) {
            // Propose an update
            std::size_t i = random_site(rng);
            std::size_t j = random_site(rng);
            std::size_t it = i+N*random_time_slice(rng);

            int de = 0;
            // Calc energy of proposed update

            // Left neighbor
            if(j > 0)
                de -= row_c[i*N+j-1] * get_sign(it,j,it,j-1);
            // right neighbor
            if(j < N-1)
                de -= row_c[i*N+j] * get_sign(it,j,it,j+1);
            // top neighbor
            if(i > 0)
                de -= col_c[(i-1)*N+j] * get_sign(it,j,it-1,j);
            // bottom neighbor
            if(i < N-1)
                de -= col_c[i*N+j] * get_sign(it,j,it+1,j);
            // front+back neighbor
            int back = it + N;
            if(back >= spin_rows.size()) back -= spin_rows.size(); // We have PBC along the imaginary time direction
            int front = it - N;
            if(front < 0) front += spin_rows.size();
            int di = ((get_sign(it,j,back,j)+get_sign(it,j,front,j))/2); // " "

            if(2*alpha*delta*de - j_t*2*2*di <= 0.0 || rnd(rng) < rs_acc_prop[de+4]*it_acc_prop[di+1] )
                spin_rows[it] ^= (uint64_t(1)<<j); // Flip the spin
        }
    }

    energy_t energy() const {
        energy_t e = 0;

        // 0th row
        for(std::size_t j=0; j < N-1; ++j)
            // energy with right neighbor
            e += row_c[j] * get_sign(0,j,0,j+1);

        // bulk
        for(std::size_t i = 1; i < N; ++i) {
            e += col_c[(i-1)*N+(N-1)] * get_sign(i,N-1,i-1,N-1);
            for(std::size_t j=0; j < N-1; ++j) {
                // energy with right neighbor
                e += row_c[i*N+j] * get_sign(i,j,i,j+1);
                // energy with bottom neighbor
                e += col_c[(i-1)*N+j] * get_sign(i,j,i-1,j);
            }
        }
        return e;
    }

    void run(double beta, double delta_t) {
        init();
        for(double t=0; t <= 1.0; t += delta_t) {
            double field = 1.0 - t;
            double alpha = t;
            sweep(beta, alpha, field);
        }
    }
  private:
    std::mt19937 rng;
    std::uniform_real_distribution<> rnd;
    std::uniform_int_distribution<> random_site;
    std::uniform_int_distribution<> random_time_slice;
    std::vector<short> const row_c;
    std::vector<short> const col_c;
    std::vector<uint64_t> spin_rows;
    std::size_t const N;
    std::size_t const M;
};

}
#endif //SIM_QUANTUM_ANNEALER_HPP
