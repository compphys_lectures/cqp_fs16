import matplotlib.pyplot as plt
import re


f = open("succ_rates-quantum.txt", "r")
pat = re.compile("beta = ([\d|\.]*) dt = ([\d|\.]*) rate = ([\d|\.]*) \+\/\- ([\d|\.]*)")

data = {}
for l in f:
    fnd = pat.search(l)
    if(fnd):
        beta = float(fnd.group(1))
        dt   = float(fnd.group(2))
        rate = float(fnd.group(3))
        rerr = float(fnd.group(4))
        if(dt not in data):
            data[dt] = [[beta],[rate],[rerr]]
        else:
            data[dt][0] += [beta]
            data[dt][1] += [rate]
            data[dt][2] += [rerr]
plt.figure()
plt.xscale('log')

for d in sorted(data.keys()):
    label = r'$\Delta s = ' + ('%f' % d).rstrip('0') + '$'
    plt.errorbar(data[d][0], data[d][1], data[d][2], fmt="o--", label=label)
plt.title(r'Success rate of the quantum annealer (1000 runs)')
plt.xlabel(r'$\beta$')
plt.ylabel(r'Success Rate')
plt.legend(loc='best')
plt.savefig('succ_rates-quantum.png')
plt.show()
