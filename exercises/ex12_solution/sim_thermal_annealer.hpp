#ifndef SIM_THERMAL_ANNEALER_HPP
#define SIM_THERMAL_ANNEALER_HPP
#include <vector>
#include <random>
#include <assert.h>

namespace cqp {

class thermal_annealer {
  public:
    typedef int energy_t;

    thermal_annealer(std::size_t N, std::vector<short> const& row_c, std::vector<short> const& col_c, unsigned int seed = 5000)
    : rng(seed), random_site(0,N-1), row_c(row_c), col_c(col_c), spin_rows(N), N(N) {
        // We also assume row_c and col_c have only +1,-1 elements
        assert(N < 64);
        init();
    }

    void init() {
        // Generate random spin config
        for(std::size_t i = 0; i < N; ++i) {
            spin_rows[i] = rng();
            spin_rows[i] <<= 32;
            spin_rows[i] |= rng();
        }
    }

    inline int64_t get_sign(std::size_t i1, std::size_t j1, std::size_t i2, std::size_t j2) const {
        assert(i1 < spin_rows.size());
        assert(i2 < spin_rows.size());
        assert(j1 < 64);
        assert(j2 < 64);
        return 2*(((spin_rows[i1] >> j1) ^ (spin_rows[i2] >> (j2))) & int64_t(1))-1;
    }

    void sweep(double beta) {
        double acc_prop[9];
        for(int i=0; i < 9; ++i)
            acc_prop[i] = std::exp( 2*beta*(4-i));

        for(std::size_t n = 0; n < N*N; ++n) {
            // Propose an update
            std::size_t i = random_site(rng);
            std::size_t j = random_site(rng);


            int de = 0;
            // Calc energy of proposed update
            assert(i*N+j    < row_c.size());

            // Left neighbor
            if(j > 0)
                de -= row_c[i*N+j-1] * get_sign(i,j,i,j-1);
            // right neighbor
            if(j < N-1)
                de -= row_c[i*N+j] * get_sign(i,j,i,j+1);
            // top neighbor
            if(i > 0)
                de -= col_c[(i-1)*N+j] * get_sign(i,j,i-1,j);
            // bottom neighbor
            if(i < N-1)
                de -= col_c[i*N+j] * get_sign(i,j,i+1,j);

            if(de <= 0 || rnd(rng) < acc_prop[de+4] )
                spin_rows[i] ^= (uint64_t(1)<<j); // Flip the spin
        }
    }

    energy_t energy() const {
        energy_t e = 0;

        // 0th row
        for(std::size_t j=0; j < N-1; ++j)
            // energy with right neighbor
            e += row_c[j] * get_sign(0,j,0,j+1);

        // bulk
        for(std::size_t i = 1; i < N; ++i) {
            e += col_c[(i-1)*N+(N-1)] * get_sign(i,N-1,i-1,N-1);
            for(std::size_t j=0; j < N-1; ++j) {
                // energy with right neighbor
                e += row_c[i*N+j] * get_sign(i,j,i,j+1);
                // energy with bottom neighbor
                e += col_c[(i-1)*N+j] * get_sign(i,j,i-1,j);
            }
        }
        return e;
    }

    void run(double delta_beta, double beta = 1e-5, double beta_max = 5.0) {
        init();
        for(; beta < beta_max; beta += delta_beta) {
            sweep(beta);
        }
    }
  private:
    std::mt19937 rng;
    std::uniform_real_distribution<> rnd;
    std::uniform_int_distribution<> random_site;
    std::vector<short> const row_c;
    std::vector<short> const col_c;
    std::vector<uint64_t> spin_rows;
    std::size_t const N;
};

}
#endif //SIM_THERMAL_ANNEALER_HPP
