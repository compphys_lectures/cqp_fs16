#include <vector>
#include <iostream>
#include <complex>
#include <string>
#include <random>


class QSimulator
{
public:
    typedef unsigned state_type;
    typedef double scalar_type;
    typedef std::complex<scalar_type> complex_type;
    typedef std::vector<complex_type> wf_type;
    typedef std::pair<scalar_type,scalar_type> dmatr_type;
    QSimulator(unsigned n, bool v);
    
    void H(const wf_type& iwf, wf_type& owf, unsigned qubitId) const;
    void CNOT(const wf_type& iwf, wf_type& owf, unsigned cqid, unsigned qid) const;
    void Z(wf_type& wf, unsigned qid) const;
    void X(const wf_type& iwf, wf_type& owf, unsigned qid) const;
    void Uf(const wf_type& iwf, wf_type& owf, state_type(*f)(state_type st, unsigned hqid)) const;
    
    bool measureQubit(wf_type& wf, unsigned qid) const; // physical measurement
    dmatr_type tomography(const wf_type& iwf, unsigned qid) const; // state of the qubit qid
    void printWF(wf_type wf) const;
    state_type maxState() const { return maxState_; }

private:
    const unsigned n_;
    const state_type maxState_;
    const bool verbose_;
    
    mutable std::mt19937 rndEngine_;
    mutable std::uniform_real_distribution<scalar_type> probDist_;
    mutable std::uniform_int_distribution<unsigned> qidDist_;
};

QSimulator::QSimulator(unsigned n, bool v)
:  verbose_(v), n_(n), maxState_( (1<<n)-1 ), probDist_(0.0,1.0), qidDist_(1,n-1), rndEngine_(time(NULL))
{
}

void QSimulator::H(const wf_type& iwf, wf_type& owf, unsigned qubitId) const{
    owf = iwf;
    for(state_type st=0; st<=maxState(); st++){
        if(std::abs(iwf[st])!=0.0){
            state_type flippedSt = st ^ (1<<qubitId);
            int sign = (st & (1<<qubitId)) >> qubitId; // 1 if qubitId is set 0 otherwise
            sign = 2*sign-1; // 1 if qubitId is set -1 otherwise

            owf[flippedSt] = ( static_cast<scalar_type>(sign)*iwf[flippedSt] + iwf[st] )/std::sqrt(2);
            owf[st] = ( -static_cast<scalar_type>(sign)*iwf[st] + iwf[flippedSt] )/std::sqrt(2);
        }
    }
    if(verbose_){
        std::cout << "wavefunction after the Hadamard gate:" << std::endl;
        printWF(owf);
    }
}

void QSimulator::CNOT(const wf_type& iwf, wf_type& owf, unsigned cqid, unsigned qid) const{
    owf = iwf;
    for(state_type st=0; st<=maxState(); st++){
        if(std::abs(iwf[st])!=0.0){
            if (st & (1<<cqid)){ // control qubit set
                state_type flippedSt = st ^ (1<<qid); // flip the other qubit
                owf[flippedSt] = iwf[st];
                owf[st] = iwf[flippedSt];
            }
        }
    }
    if(verbose_){
        std::cout << "wavefunction after the CNOT gate:" << std::endl;
        printWF(owf);
    }
}

// ====== convention: output qubit id=0
void QSimulator::Uf(const wf_type& iwf, wf_type& owf, state_type(*f)(state_type st, unsigned hqid) ) const{
    owf = iwf;
    unsigned hiddenControlBitId = qidDist_(rndEngine_);
    for(state_type st=0; st<=maxState(); st++){
        if(std::abs(iwf[st])!=0.0){
            if( f(st,hiddenControlBitId) ){
                state_type newState = st ^ 1; // y = y XOR f(x)
                owf[st] = iwf[newState];
                owf[newState] = iwf[st];
            }
        }
    }
    if(verbose_){
        std::cout << "owavefunction after the Uf gate:" << std::endl;
        printWF(owf);
    }
}

bool QSimulator::measureQubit(wf_type& wf, unsigned qid) const{
    dmatr_type dmatr = tomography(wf, qid);
    
    scalar_type randNum = probDist_(rndEngine_);
    bool measured1 = (randNum<dmatr.second)? 1: 0;
    if(verbose_){
        std::cout << "probability to measure |0>: " << dmatr.first << ", for |1>: " << dmatr.second << std::endl;
        std::cout << "randNum = " << randNum << ",measured1 = " << measured1 << std::endl;
    }
    
// ====== drop projected out components, renormalize the rest
    for(state_type st=0; st<=maxState(); st++){
        if(std::abs(wf[st])!=0.0){
            if(st & (1<<qid)){ // bit set
                if(measured1){
                    wf[st] /= std::sqrt(dmatr.second); // renormalize
                }else{
                    wf[st] = 0;
                }
            }else{        // bit not set
                if(measured1){
                    wf[st] = 0;
                }else{
                    wf[st] /= std::sqrt(dmatr.first); // renormalize
                }
            }
        }
    }
// ====== drop projected out components, renormalize the rest
    
    if(verbose_){
        std::cout << "wavefunction after the measurement:" << std::endl;
        printWF(wf);
    }
    
    if(measured1){
        return true;
    }else{
        return false;
    }
}

QSimulator::dmatr_type QSimulator::tomography(const wf_type& wf, unsigned qid) const{
    scalar_type beta2 = 0;
    scalar_type alpha2 = 0;
    for(state_type st=0;st<=maxState(); st++){
        if(st & (1<<qid)){ // bit set
            beta2 += std::real( std::conj(wf[st]) * wf[st] );
        }else{
            alpha2 += std::real( std::conj(wf[st]) * wf[st] );
        }
    }
    return dmatr_type(alpha2,beta2);
}

void QSimulator::X(const wf_type& iwf, wf_type& owf, unsigned qid) const{ // NOT gate
    owf = iwf;
    for(state_type st=0; st<=maxState(); st++){
            state_type newState = st ^ (1<<qid);
            owf[newState] = iwf[st];
            owf[st] = iwf[newState];
    }
    if(verbose_){
        std::cout << "wavefunction after the X gate:" << std::endl;
        printWF(owf);
    }
}

void QSimulator::Z(wf_type& wf, unsigned qid) const{
    for(state_type st=0; st<=maxState(); st++){
        if(std::abs(wf[st])!=0.0){
            if(st & (1<<qid)){
                wf[st] *= -1;
            }
        }
    }
    if(verbose_){
        std::cout << "wavefunction after the Z gate:" << std::endl;
        printWF(wf);
    }
}

void QSimulator::printWF(wf_type wf) const{
    for(state_type st=0; st<=maxState(); st++){
        std::cout << wf[st] << "\t";
    }
    std::cout << std::endl;
}


QSimulator::state_type f_const0(QSimulator::state_type st, unsigned hiddenControlBitId){
    return 0;
}
QSimulator::state_type f_const1(QSimulator::state_type st, unsigned hiddenControlBitId){
    return 1;
}
QSimulator::state_type f_balanced(QSimulator::state_type st, unsigned hiddenControlBitId){
    return (st & 1<<hiddenControlBitId)>>hiddenControlBitId;
}

std::string usage(const std::string& prog)
{
    return "usage: " + prog + " nQubitsForDeutschJosza";
}
int main(int argc, const char** argv)
{
    if( argc != 2 ){
        std::cerr << usage(argv[0]) << std::endl;
        return -1;
    }
    unsigned DJN = stoi(argv[1],nullptr,10);
    if(DJN<2){
        std::cerr << "At least 2 qubits will be needed for the Deutsch-Josza algorithm" << std::endl;
        return -1;

    }
// ============== Teleportation begin
    // qubit index convention: unknown id=0, Alice Bell id=1, Bob Bell id=2
    bool verbose = false;
    int n = 3;
    std::cout << "Teleportation:" << std::endl;
    QSimulator teleport(n, verbose);
    QSimulator::wf_type iwf(teleport.maxState()+1,0);
    QSimulator::wf_type owf(teleport.maxState()+1,0);
    
    iwf[1] = 1/3.0;
    iwf[0] = 2*std::sqrt(2)/3; // 1/3 |001> + 2sqrt(2)/3 |000>
    
    std::cout << "initial wavefunction:" << std::endl;
    teleport.printWF(iwf);
    
    QSimulator::dmatr_type dmatr = teleport.tomography(iwf, 0);
    std::cout << "State of the Alice's |psi> qubit (id=0) is " << std::sqrt(dmatr.first) << "|0> + " << std::sqrt(dmatr.second) << "|1>" << std::endl;
    
// ====== generate Bell00 state
    teleport.H(iwf,owf,1);
    iwf.swap(owf);
    teleport.CNOT(iwf,owf,1,2);
    iwf.swap(owf);
// ====== generate Bell00 state
    
    teleport.CNOT(iwf,owf,0,1);
    iwf.swap(owf);
    teleport.H(iwf,owf,0);
    iwf.swap(owf);
    bool M1 = teleport.measureQubit(iwf,0);
    bool M2 = teleport.measureQubit(iwf,1);
    if(M2){
        teleport.X(iwf,owf,2);
        iwf.swap(owf);
    }
    if(M1){
        teleport.Z(iwf,2);
    }
    
    std::cout << "final wavefunction:" << std::endl;
    teleport.printWF(iwf);
    
    dmatr = teleport.tomography(iwf, 2);
    std::cout << "State of the Bob's (id=2) qubit is " << std::sqrt(dmatr.first) << "|0> + " << std::sqrt(dmatr.second) << "|1>" << std::endl;
// ============== Teleportation end
    
// ============== Deutsch-Jozsa begin
    // convention: read off bit id=0.
    std::cout << "\n\n\nDeutsch-Jozsa with " << DJN << " qubits:" << std::endl;
    QSimulator DJozsa(DJN, false);
    QSimulator::wf_type dj_iwf(DJozsa.maxState()+1,0);
    QSimulator::wf_type dj_owf(DJozsa.maxState()+1,0);
    QSimulator::state_type initial_state = 1; // 1 in the read off bit
    dj_iwf[initial_state] = QSimulator::complex_type(1.0,0.0);
    
    for(unsigned qid = 0; qid<DJN; qid++){
        DJozsa.H(dj_iwf, dj_owf, qid);
        dj_iwf.swap(dj_owf);
    }
    DJozsa.Uf(dj_iwf, dj_owf, &f_balanced); // for balanced function
    //DJozsa.Uf(dj_iwf, dj_owf, &f_const1); // for constant function
    dj_iwf.swap(dj_owf);
    for(unsigned qid = 1; qid<DJN; qid++){
        DJozsa.H(dj_iwf, dj_owf, qid);
        dj_iwf.swap(dj_owf);
    }
    bool allZeros = true;
    for(unsigned qid = 1; qid<DJN; qid++){
        allZeros = allZeros && (!DJozsa.measureQubit(dj_iwf,qid));
        if(!allZeros){
            break;
        }
    }
    if(allZeros){
        std::cout << "the function f is constant" << std::endl;
    }else{
        std::cout << "the function f is balanced" << std::endl;
    }
// ============== Deutsch-Jozsa end
}
