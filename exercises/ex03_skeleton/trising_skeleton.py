# ---- CQP FS16, Ex3.1 skeleton ----
# ----------------------------------
import numpy as np
import matplotlib.pyplot as plt

def evolve(l, j, hx, v, dt, n):
    """ Evolve the state v in time by n * dt 
    
    Parameters:
    l  length of chain
    j  Ising coupling for each neighbors, 1-d numpy array with length at least l-1
    hx transverse field for each site, 1-d numpy array with length at least l
    v  the state at time t, complex 1-d numpy array with length at least 1 << l
    dt timestep
    n  number of consecutive timesteps

    Return:
    v  the state at time t+n*dt
    """
    # Implement the time evolution of the state v here.
    # Use the split-operator scheme we discussed in the
    # exercise class.
    return v
    
    
def magnetization(v, l):
    """ Measure the magnetization per site for the state v 
    
    Parameters:
    v  the state to be measured, complex 1-d numpy array with length at least 1 << l
    l  length of chain

    Return:
    m  the magnetization at each site, 1-d numpy array with length l
    """
    # Implement the measurement of the magnetization here.
    return m
    


l   =  9 # length of chain
dim =  1 << l
j   =  1.00*np.ones((l)) # Ising coupling
hx  =  0.40*np.ones((l-1)) # transverse field strength Gamma

# Starting configuration
ind = int('000010000',base=2) # specify spin configuration: 0 = down, 1 = up
v   = np.zeros((dim),dtype=complex)
v[ind] = 1. # start time evolution with a single basis state

# Parameters for time evolution
tend   = 10. # duration of the time evolution
tmeas  = 0.5 # time after which a measurment is performed
dt     = 0.1 # time step used for the evolution
nmeas  = int((tend)/tmeas) # number of measurements
nstep  = int((tmeas)/dt) # number of steps between measurements

# measure magnetization
m=np.zeros((nmeas,l));
m[0,:] = magnetization(v, l)

# Do time evolution
for t in range(1,nmeas):
    print(t*tmeas)
    v=evolve(l,j,hx,v,dt,nstep)
    m[t,:] = magnetization(v, l)

# plot magnetization
plt.pcolor(np.array(range(l+1)),np.linspace(0.,tend,nmeas),m,
           vmin=-1.0, vmax=1.0)
plt.xlabel('site')
plt.ylabel('time')
plt.axis([0,l,0,tend])
plt.title("Magnetisation, J = "+str(j[0])+" , hx = "+str(hx[0]))
plt.colorbar()
plt.show()
