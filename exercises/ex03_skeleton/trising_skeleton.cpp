// ---- CQP FS16, Ex 3.1 Skeleton code ----
// ----------------------------------------
#include <valarray>
#include <vector>
#include <iostream>
#include <cmath>
#include <complex>

typedef std::valarray< std::complex<double> > state_vector_t;
typedef std::vector<double> param_vector_t;
typedef unsigned state_t;

// Implement the time evolution of state x by n*dt here. Use the split
// operator scheme we discussed in the exercise class.
void evolve(unsigned l, const param_vector_t &j, const param_vector_t &hx, double dt, unsigned n, state_vector_t& x)
{
// ........
}

void printMagnetization(unsigned l, const state_vector_t& v, std::ostream& outputStream)
{
  std::vector<double> res(l,0.0);
  for(int r=0; r<l; r++){
    double m(0);
    // Measure magnetization at site r here. To be completed.
    outputStream << m << "\t\t";
  }
  outputStream << std::endl;
}

int main(){
  // ======== input
  unsigned l = 15; // length of chain
  param_vector_t j(l-1,0.0); // Ising coupling
  param_vector_t hx(l,0.4); // transverse magnetic field
  double tmax = 20.; // maximum time reached in time evolutin
  double tmeas = 0.5; // time after which a measurment is performed
  double dt = 0.1; // time step used for the evolution
  unsigned nmeas = tmax/tmeas; // number of measurements
  unsigned nstep = tmeas/dt; // number of steps between measurements
  // ======== input

  // Starting configuration
  state_t initialInd = 1<<7;
  state_vector_t x(0., 1 << l);
  x[initialInd] = 1;   
  printMagnetization(l,x,std::cout);

  // Do time evolution
  for(int n=1; n<=nmeas; ++n){    
    evolve(l, j, hx, dt, nstep, x);
    printMagnetization(l,x,std::cout);    
  }
  
  return 0;
}
