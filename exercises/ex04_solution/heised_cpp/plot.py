import numpy as np
import matplotlib.pyplot as plt

tasks = [('open','openchain.dat'),('periodic','periodicchain.dat')]

# create two figures
plt.figure(1)
plt.title('Ground state energy per site')
plt.xlabel('$1/L$')
plt.ylabel('$E_0/L$')
plt.figure(2)
plt.title('Gap')
plt.xlabel('$1/L$')
plt.ylabel('$E_1-E_0$')
plt.xlim(0,0.3)
plt.ylim(0,0.8)

for t in tasks:
    # load data
    name = t[0]
    data = np.loadtxt(t[1])
    lengths = data[:,0]
    invlen = 1./lengths
    e0 = data[:,1]
    e1 = data[:,2]
    
    # plot ground state energy per site
    gs = e0/lengths
    plt.figure(1)
    plt.plot(invlen,gs,label=name)
    plt.legend(loc='upper left')
    
    # restrict to chains of even length
    even = lengths%2 == 0
    invlen = invlen[even]
    gap = (e1-e0)[even]
    
    # fit to form y = m*x = m/L for the 4 largest chains
    numpoints = 4
    m = np.linalg.lstsq(np.vstack(invlen[-numpoints:]),gap[-numpoints:])[0]
    
    # plot gap and 1/L fit
    plt.figure(2)
    plt.plot(invlen,gap,'.',label=name)
    invlen = np.concatenate((invlen,[0.]))
    plt.plot(invlen,m*invlen)
    plt.legend(loc='upper left')
plt.show()