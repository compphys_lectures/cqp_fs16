import numpy as np
import matplotlib.pyplot as plt

magn = np.loadtxt('magnetization.txt')
Nt   = magn.shape[0]
N    = magn.shape[1]
dt   = 0.5

fig = plt.figure()
plt.pcolor(range(N+1),np.arange(Nt+1)*dt,magn)
plt.xlim([0,N])
plt.ylim([0,Nt*dt])
plt.xlabel('site')
plt.ylabel('time')
plt.colorbar()
plt.show()
fig.savefig('magnetization.png')
