#include <iostream>
#include <fstream>
#include <stdexcept>
#include "sim_quantum_annealer.hpp"


void load(std::string const& filename, std::size_t& N, int& exact_energy, std::vector<short>& row_couplings, std::vector<short>& col_couplings) {
    std::ifstream in(filename.c_str());
    in >> exact_energy;
    in >> N;
    row_couplings.resize(N*N);
    col_couplings.resize(N*N);
    for(std::size_t i=0; i < N*N; ++i)
        in >> row_couplings[i];
    for(std::size_t i=0; i < N*N; ++i)
        in >> col_couplings[i];
}

int main() {
    int const copies = 1000;
    int exact_energy = 0.0;
    std::size_t N = 10;
    std::vector<short> row_couplings;
    std::vector<short> col_couplings;
    load("instance.txt", N, exact_energy, row_couplings, col_couplings);


    for(double beta=4e-2; beta <= 11; beta *= 2)
    for(double dt=1e-5; dt <= 1e-1 ; dt *= 10) {
        int minimum = 0;
        int gs_hits = 0;
        for(int i = 0 ; i < copies; ++i) {
            cqp::quantum_annealer annealer(N,row_couplings, col_couplings, 10, 23*i);
            annealer.run(beta, dt); {
                std::cout <<"Run " << i <<": E = " <<annealer.energy() << std::endl;
                minimum = (std::min)(minimum,annealer.energy());
                if(annealer.energy() == exact_energy)
                    ++gs_hits;
            }
        }

        double rate = static_cast<double>(gs_hits) / copies;
        double err  = std::sqrt((static_cast<double>(gs_hits) / copies - rate*rate)/(copies-1));
        std::cout<<"exact         E = "<< exact_energy <<std::endl;
        std::cout<<"annealing min E = "<< minimum << std::endl;
        std::cout<<"%:                "<< rate << "+/-" << err << std::endl;
        std::cerr<<"beta = " << beta << " dt = " << dt << " rate = "<< rate << " +/- " << err<< std::endl;
    }
    return 0;
}
