#ifndef SIM_THERMAL_ANNEALER_HPP
#define SIM_THERMAL_ANNEALER_HPP
#include <vector>
#include <random>
#include <assert.h>

namespace cqp {

class thermal_annealer {
  public:
    typedef int energy_t;

    thermal_annealer(std::size_t N, std::vector<short> const& row_c, std::vector<short> const& col_c, unsigned int seed = 5000)
    : rng(seed), random_site(0,N-1), row_c(row_c), col_c(col_c), spin_rows(N), N(N) {
        // We also assume row_c and col_c have only +1,-1 elements
        assert(N < 64);
        init();
    }

    void init() {
        // Generate random spin config
        for(std::size_t i = 0; i < N; ++i) {
            spin_rows[i] = rng();
            spin_rows[i] <<= 32;
            spin_rows[i] |= rng();
        }
    }

    inline int64_t get_sign( /* TODO */ ) const {


        /* TODO */
        return 0;


    }

    void sweep(double beta) {


        /* TODO */


    }

    energy_t energy() const {
        energy_t e = 0;


        /* TODO */


        return e;
    }

    void run(double delta_beta, double beta = 1e-5, double beta_max = 5.0) {
        init();
        for(; beta < beta_max; beta += delta_beta)
            sweep(beta);
    }
  private:
    std::mt19937 rng;
    std::uniform_real_distribution<> rnd;
    std::uniform_int_distribution<> random_site;
    std::vector<short> const row_c;
    std::vector<short> const col_c;
    std::vector<uint64_t> spin_rows;
    std::size_t const N;
};

}
#endif //SIM_THERMAL_ANNEALER_HPP
