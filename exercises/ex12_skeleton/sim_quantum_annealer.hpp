#ifndef SIM_QUANTUM_ANNEALER_HPP
#define SIM_QUANTUM_ANNEALER_HPP
#include <vector>
#include <limits>
#include <random>
#include <assert.h>


namespace cqp {

class quantum_annealer {
  public:
    typedef int energy_t;

    quantum_annealer(std::size_t N, std::vector<short> const& row_c, std::vector<short> const& col_c, std::size_t M, unsigned int seed = 3000)
    : rng(seed), random_site(0,N-1), random_time_slice(0,M-1), row_c(row_c), col_c(col_c), spin_rows(N*M), N(N), M(M) {
        // We also assume row_c and col_c have only +1,-1 elements
        assert(N < 64);
        init();
    }

    void init() {
        // Generate random spin config
        for(std::size_t i = 0; i < spin_rows.size(); ++i) {
            spin_rows[i] = rng();
            spin_rows[i] <<= 32;
            spin_rows[i] |= rng();
        }
    }

    // +1 if spins are antialigned, -1 if spins are aligned
    inline int64_t get_sign( /* TODO */ ) const {


        /* TODO */
        return 0;


    }

    void sweep(double beta, double alpha, double field) {

        double delta = beta / M;
        double j_t = std::numeric_limits<double>::max(); // limit of the coupling strength as gamma ->0 (all the replicas are perfectly coupled)


        /* TODO */


    }

    energy_t energy() const {
        energy_t e = 0;


        /* TODO */


        return e;
    }

    void run(double beta, double delta_t) {
        init();
        for(double t=0; t <= 1.0; t += delta_t) {


            /* TODO */


        }
    }
  private:
    std::mt19937 rng;
    std::uniform_real_distribution<> rnd;
    std::uniform_int_distribution<> random_site;
    std::uniform_int_distribution<> random_time_slice;
    std::vector<short> const row_c;
    std::vector<short> const col_c;
    std::vector<uint64_t> spin_rows;
    std::size_t const N;
    std::size_t const M;
};

}
#endif //SIM_QUANTUM_ANNEALER_HPP
