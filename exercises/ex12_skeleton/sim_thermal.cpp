#include <iostream>
#include <fstream>
#include <stdexcept>
#include "sim_thermal_annealer.hpp"


void load(std::string const& filename, std::size_t& N, int& exact_energy, std::vector<short>& row_couplings, std::vector<short>& col_couplings) {
    std::ifstream in(filename.c_str());
    in >> exact_energy;
    in >> N;
    row_couplings.resize(N*N);
    col_couplings.resize(N*N);
    for(std::size_t i=0; i < N*N; ++i)
        in >> row_couplings[i];
    for(std::size_t i=0; i < N*N; ++i)
        in >> col_couplings[i];
}

int main() {
    int minimum = 0;
    int const copies = 100;
    int gs_hits = 0;


    int exact_energy = 0.0;
    std::size_t N = 10;
    std::vector<short> row_couplings;
    std::vector<short> col_couplings;
    load("instance.txt", N, exact_energy, row_couplings, col_couplings);

    cqp::thermal_annealer annealer(N,row_couplings, col_couplings);

    for(int i = 0 ; i < copies; ++i) {
        annealer.run(1e-5, 0.1, 5.0);
        std::cout <<"Run " << i <<": E = " <<annealer.energy() << std::endl;
        minimum = (std::min)(minimum,annealer.energy());
        if(annealer.energy() == exact_energy)
            ++gs_hits;
    }

    std::cout<<"exact         E = "<< exact_energy <<std::endl;
    std::cout<<"annealing min E = "<< minimum << std::endl;
    std::cout<<"%:                "<< static_cast<double>(gs_hits)/copies << std::endl;
    return 0;
}
