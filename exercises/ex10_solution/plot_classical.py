import sys, os, re

import numpy as np
import matplotlib.pyplot as plt
import pyalps
from pyalps.plot import plot

files = pyalps.getResultFiles(dirname='data')
data = pyalps.loadMeasurements(files , ['|m|','m^2'])

for d in pyalps.flatten(data):
    if re.search(r'_local\.h5$',d.props['filename']):  
        d.props['update'] = 'local'
    else:
        d.props['update'] = 'cluster'
    

m = pyalps.collectXY(data, 'Jx', '|m|', foreach=['update'])
m2 = pyalps.collectXY(data, 'Jx', 'm^2', foreach=['update'])

plt.figure()
plot(m)
plt.xlabel('$\\beta J$')
plt.ylabel('|magnetization|')
plt.legend(loc='best', frameon=False)
plt.savefig('classical_ising-mag.pdf',transparent=True)


plt.figure()
plot(m2)
plt.xlabel('$\\beta J$')
plt.ylabel('|magnetization|^2')
plt.legend(loc='best', frameon=False)
plt.savefig('classical_ising-mag2.pdf',transparent=True)

plt.figure()
for d in m: 
    plt.plot(d.x,[y.error for y in d.y],label=d.props['label'])
plt.xlabel('$\\beta J$')
plt.ylabel('error')
plt.legend(loc='best', frameon=False)
plt.savefig('classical_ising-mag_error.pdf',transparent=True)

plt.figure()
for d in m: 
    plt.plot(d.x,[y.tau for y in d.y],label=d.props['label'])
plt.xlabel('$\\beta J$')
plt.ylabel('autocorrelation time')
plt.legend(loc='best', frameon=False)
plt.savefig('classical_ising-mag_tau.pdf',transparent=True)


plt.show()
