#!/bin/sh

L=16
N=1000000

for J in 0.1 0.2 0.3 0.4 0.44 0.5 0.6 0.7 0.8 0.9
do
    time ../ising $L $L $J $J $N &
    time ../ising $L $L $J $J $N local &
    wait
done
